<?php
$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

if( $lang == 'fr' ){
	header( 'Location: http://toyotatestdays.be/fr/index.html' ) ;
} elseif( $lang == 'nl' ){
	header( 'Location: http://toyotatestdays.be/testwagens.html' ) ;
} else {
	header( 'Location: http://toyotatestdays.be/testwagens.html' ) ;
}
?>