$(document).ready(function() {
    
    $('.fade').hover(
        function(){
            $(this).find('.caption').fadeIn(250);
            $(this).addClass('hover');
        },
        function(){
            $(this).find('.caption').fadeOut(250);
            $(this).removeClass('hover');
        }
    );

});